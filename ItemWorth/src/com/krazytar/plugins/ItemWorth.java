package com.krazytar.plugins;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class ItemWorth extends JavaPlugin implements Listener {
    public static final Logger log = Bukkit.getLogger();
    FileConfiguration config;
    public static Economy econ = null;
    String usage = ChatColor.RED + "Do: /money set <amount>";
    String usage2 = ChatColor.RED + "Usage: /iw give <player> <item> <worth>";
    
    @Override
    public void onEnable() {
        saveDefaultConfig();
        config = getConfig();
        config.options().copyDefaults(true);
        saveConfig();
        getServer().getPluginManager().registerEvents(this, this);
        if(!setupEconomy()) {
            log.severe("Disabled due to no Vault dependency found");
        }
    }
    
    // The commands
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(label.equalsIgnoreCase("iw")) {
            if(args.length != 0) {
                if(args[0].equalsIgnoreCase("set")) {
                    if(sender.hasPermission("iw.set")) {
                        if(sender instanceof Player) {
                            if(args.length != 1) {
                                if(args[1] != null && isNumeric(args[1])) {
                                    Player p = (Player) sender;
                                    ItemStack i = p.getItemInHand();
                                    if(i == null || i.getType() == Material.AIR) {
                                        sender.sendMessage(config.getString("no-item-in-hand"));
                                        return true;
                                    }
                                    setWorth(i, Double.parseDouble(args[1]));
                                    return true;
                                } else {
                                    sender.sendMessage(usage);
                                    return true;
                                }
                            } else {
                                sender.sendMessage(usage);
                                return true;
                            }
                        } else {
                            sender.sendMessage(config.getString("sender-is-not-player"));
                            return true;
                        }
                    } else {
                        sender.sendMessage(config.getString("no-permissions"));
                        return true;
                    }
                } else if(args[0].equalsIgnoreCase("give")) {
                    if(sender.hasPermission("iw.give")) {
                        if(args.length >= 3) {
                            if(getServer().getPlayer(args[1]) != null) {
                                if(Material.getMaterial(args[2]) != null && Material.getMaterial(args[1]) != Material.AIR) {
                                    if(isNumeric(args[3])) {
                                        Player player = getServer().getPlayer(args[1]);
                                        args[2] = args[2].toUpperCase();
                                        ItemStack is = new ItemStack(Material.getMaterial(args[2]));
                                        setWorth(is, Double.parseDouble(args[3]));
                                        player.getInventory().addItem(is);
                                        sender.sendMessage(ChatColor.GREEN + "Successfully gave " + player.getName() + " " + Material.getMaterial(args[2]).name() + " with a worth of " + args[3]);
                                        return true;
                                    } else {
                                        sender.sendMessage(usage2);
                                        return true;
                                    }
                                } else {
                                    sender.sendMessage(usage2);
                                    return true;
                                }
                            } else {
                                sender.sendMessage(config.getString("player-not-online"));
                                return true;
                            }

                        } else {
                            sender.sendMessage(usage2);
                            return true;
                        }
                    } else {
                        sender.sendMessage(config.getString("no-permissions"));
                        return true;
                    }
                }
            }
            sender.sendMessage(ChatColor.AQUA + "Plugin created by: Javy_K");
            sender.sendMessage(ChatColor.GRAY + "---Commands---");
            sender.sendMessage(ChatColor.GOLD + "/iw - The base command for the plugin");
            sender.sendMessage(ChatColor.GOLD + "/iw set <amount> - Set the amount an item is worth");
            sender.sendMessage(ChatColor.GOLD + "/iw give <player> <item> <worth> - Gives the player an item with the specified worth");
            sender.sendMessage(ChatColor.GRAY + "---Permissions---");
            sender.sendMessage(ChatColor.GOLD + "iw.set - Allows you to set the worth of an item with a command - Default: OP");
            sender.sendMessage(ChatColor.GOLD + "iw.use - When you right click with an item that has worth it gives you money - Default: OP");
            sender.sendMessage(ChatColor.GOLD + "iw.give - Allows you to give players an item of a specified worth - Default: OP");
            sender.sendMessage(ChatColor.AQUA + "Bugs? Errors? Glitches? PM me on the bukkit forums. Bukkit forum username: KrazyRaven");
        }
        
        
        return false;
    }
    
    
    
    
    
    
    // Get money on right click
    @EventHandler
    public void onPlayerRightClick(PlayerInteractEvent e) {
        if(e.getPlayer().hasPermission("iw.use")) {
            ItemStack i = e.getItem();
            if(containsWorth(i)) {
                econ.depositPlayer(e.getPlayer(), getWorth(i));
                e.getPlayer().getInventory().remove(i);
                e.getPlayer().sendMessage(config.getString("receive-money"));
                e.setCancelled(true);
            }
        }
    }
    // Set item worth
    public void setWorth(ItemStack i, double value) {
        List<String> lore = new ArrayList<>();
        ItemMeta im = i.getItemMeta();
        if(value <= 0) {
            // Do nothing
        } else {
           lore.add(Double.toString(value)); 
        }
        im.setLore(lore);
        i.setItemMeta(im);
    }
    // Get item worth
    public double getWorth(ItemStack i) {
        List<String> lore;
        lore = i.getItemMeta().getLore();
        if(lore.size() > 0) {
            double amount = Double.parseDouble(lore.get(0));
            amount *= i.getAmount();
            return amount;
        }
        return 0;
    }
    
    // Item has worth
    public boolean containsWorth(ItemStack i) {
        if(i == null) {
            return false;
        }
        ItemMeta im;
        if(i.hasItemMeta()) {
            im = i.getItemMeta();
        } else {
            return false;
        }
        if(im.getLore().size() > 0) return isNumeric(im.getLore().get(0));
        return false;
    }
    
    // Check if String is numeric
    public static boolean isNumeric(String str)  {  
        try  {  
            double d = Double.parseDouble(str);  
        }  
        catch(NumberFormatException nfe) {  
            return false;  
        }  
        return true;  
    }
    
    // Vault economy setup
    private boolean setupEconomy() {
        if(getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if(rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
    }
